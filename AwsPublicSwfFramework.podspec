
Pod::Spec.new do |s|

  s.name         = "AwsPublicSwfFramework"
  s.version      = "0.0.1"
  s.summary      = "A short description of AwsPublicSwfFramework."

  s.description  = "A large description of AwsPublicSwfFramework. Used For Amazon Swf"

  s.homepage     = "https://sahajbhaikanhaiya@bitbucket.org/sahajbhaikanhaiya/swfframeworkopen"

  s.license      = "MIT"

  s.author        = { "AwsPublicSwfFramework" => "sahaj.bhaikanhaiya@gmail.com" }
 

  s.source       = { :git => "https://sahajbhaikanhaiya@bitbucket.org/sahajbhaikanhaiya/AwsPublicSwfFramework.git", :tag => s.version }

  s.ios.deployment_target = '9.0'

  s.source_files  = "AwsPublicSwfFramework", "AwsPublicSwfFramework/*.{h,m,swift}"
  s.dependency 'SWFFramework'


end
